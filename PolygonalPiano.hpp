///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////


// PianoKey and PolygonalPiano are defined here. The concept of the  //
// Polygonalpiano object is a playable Keyboard showing a different  //
// regular polygon for each note on every octave of the 88keys piano.//
// The color of each note is assigned in the same way as for the     //
// SoundColor object and its derived ones.                           //
// The piano Samples used in this project are available in the site  //
// of University of Iowa http://theremin.music.uiowa.edu/MIS.html    //
// together with a lot of fantastic free instrument samples.         //

 
#ifndef POLYGONALPIANO_HPP
#define POLYGONALPIANO_HPP

#include <TGUI/TGUI.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include "SfmlSoundColor.hpp"
#include <vector>
#include <cmath>
#include <string>


enum note {C=1, Db, D, Eb, E, F, Gb, G, Ab, A, Bb, B};
enum OctaveCases {First, Other, Last};

// This is the single key of the keyboard. It contains information   //
// about the note and the octave and it provides a sound and a shape.//

class PianoKey

{

public:

// constructor

// Explicit constructor                                              //
// arguments:                                                        //
// octave: the number representing the octave on the piano keyboard. //
//         The first octave begins with the first C. The 0th octave  //
//         is only made by three notes (A0,Bb0,B0).                  //
// Note:   the number (note type) representing the note in the octave//
          
  PianoKey(int octave, note Note) : m_octave(octave), m_note(Note) {
  Polygon.setPointCount(GetKeyNumber()); 
  }

// Default constructor                                               //
    
  PianoKey() {}

// Acces methods

// Method that returns the octave of the key.                        //

  int   GetOctave()    {return m_octave;}

// Method that returns the note of the key.                          //

  note  GetNote()      {return m_note;}

// Method that returns the corresponding number of the key. Note that//
// the enumeration starts from -2 for the A0 key.                    //
  
  int   GetKeyNumber();
  
// Method that returns the frequency of the sound.                   //
    
  float GetSoundFreq();
  
// Set methods                                                       //

// Method that sets the octave of the key.                           //
// argument:                                                         //
// octave: the value to set as the octave number.                    //

  void SetOctave(int octave) {m_octave = octave;}

// Method that sets the note of the key.                             //
// argument:                                                         //
// Note: the value to set as the note.                               //

  void SetNote(note Note)    {m_note = Note;}

// The sf::Sound and sf::CircleShape are defined with public         //
// accessibility in order to grant easy access to their methods by   //
// PolygonalPiano class.                                             //

  sf::Sound       KeySound;
  sf::CircleShape Polygon;
    
  private:

// The fundamental information of the object: the octave and the note//
  int             m_octave;
  note            m_note;

   
};

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

// This is the full keyboard. Indeed it consists of a vector of      //
// PianoKey objects. There is also a vector of buffers needed to load//
// the samples file to play.                                         //

class PolygonalPiano
{
  public:
// Default constructor. it generates the Keyboard, i.e. fill the     //
// m_Keyboard vector.                                                //

  PolygonalPiano() {m_GeneratePiano();}
  
// The principal and most important method of the class. It contains //
// the window loop and the code that analises the user input.        //
// All these arguments are needed in order to manage the window and  //
// the gui of the program that calls this method                     //

  void Play(sf::RenderWindow &PianoWindow, sf::Event &Event, tgui::Gui  &mainGui, sf::Font &font, tgui::Theme::Ptr theme,mode &Mode); 

  private:

// Private method that fill the m_Keyboard vector with the right     //
// PianoKey objects.                                                 //
  
  void m_GeneratePiano();

// The actual "Keyboard"                                             //

  std::vector<PianoKey>        m_Keyboard;

// Thi vector of Soundbuffer is used to load a single octave of      //
// samples files.                                                    //

  std::vector<sf::SoundBuffer> m_OctaveArray;

};


#endif



