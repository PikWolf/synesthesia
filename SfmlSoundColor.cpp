#ifndef SFMLSOUNDCOLOR_CPP
#define SFMLSOUNDCOLOR_CPP

#include "SfmlSoundColor.hpp"



  SfmlSoundColor::SfmlSoundColor(float SoundFreq)
    {
     float ColorWL; 
     
     ColorWL=ligthWLFromSoundFreq(SoundFreq);
     ColorWLtoRGBA(ColorWL,m_RGBA);
     m_Color.r = m_RGBA[0];
     m_Color.g = m_RGBA[1];
     m_Color.b = m_RGBA[2];
     m_Color.a = m_RGBA[3]; 
    }
    
  void SfmlSoundColor::SetSoundFreq(float SoundFreq)
    {
      float WL; 
      
      WL=ligthWLFromSoundFreq(SoundFreq);
      ColorWLtoRGBA(WL,m_RGBA);
      m_Color.r = m_RGBA[0]; 
      m_Color.g = m_RGBA[1];
      m_Color.b = m_RGBA[2];
      m_Color.a = m_RGBA[3];
    }
#endif