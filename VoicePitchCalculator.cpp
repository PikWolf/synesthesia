///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////

#ifndef VOICEPITHCHCALCULATOR_CPP
#define VOICEPITHCHCALCULATOR_CPP

#include "VoicePitchCalculator.hpp"


const double pi = 3.141592653589793238463;

void fillZeros(std::vector<double>& array, size_t *length)
{
  size_t power=2;
  //calculate the next 2 power
  while (*length>power)
  { 
    power*=2;
  }
  
  for (int i=0; i<(power-*length); ++i)
    {
      array.push_back(0);
    }
  *length=power;
}


// The implementation of the fft algorithm has been (strongly) inspired by the manual Numerical Recipes third edition


void ComplexFFT(std::vector<double>& data,const int isign)
{
//variables for trigonometric recurrences
    unsigned long n,nn,mmax,m,j,istep,i;
    double wtemp,wr,wpr,wpi,wi,theta,tempr,tempi;


n=data.size()/2;

nn=n<<1;


j=1;
for (i=1;i<nn;i+=2) {
    if (j > i) {
   
        std::swap(data[j-1],data[i-1]);
        std::swap(data[j],data[i]);
        }
    m=n;
    while (m >= 2 && j > m) {
        j -= m;
        m >>= 1;
    }
    j += m;
}
  
  
//Danielson-Lanzcos routine
mmax=2;
//external loop
while (nn > mmax)
{
    istep = mmax<<  1;
    theta=isign*(2*pi/mmax);
    wtemp=sin(0.5*theta);
    wpr = -2.0*wtemp*wtemp;
    wpi=sin(theta);
    wr=1.0;
    wi=0.0;
    //internal loops
    for (m=1;m<mmax;m+=2) {
        for (i= m;i<=n;i+=istep) {
            j=i+mmax;
            tempr=wr*data[j-1]-wi*data[j];
            tempi=wr*data[j]+wi*data[j-1];
            data[j-1]=data[i-1]-tempr;
            data[j]=data[i]-tempi;
            data[i-1] += tempr;
            data[i] += tempi;
        }
        wr=(wtemp=wr)*wpr-wi*wpi+wr;
        wi=wi*wpr+wtemp*wpi+wi;
    }
    mmax=istep;
}
}

void RealFFT(std::vector<double>& realData)
{ 
  //Variables needed
  float c1=0.5,c2=-0.5,h1r,h1i,h2r,h2i,wr,wi,wpr,wpi,wtemp;
  int n=realData.size(),i1,i2,i3,i4;
  double theta=pi/double(n>>1);

  
  ComplexFFT(realData,1);
  
  wtemp=sin(0.5*theta);
  wpr=- 2.0*wtemp*wtemp;
  wpi=sin(theta);
  wr=1.0+wpr;
  wi=wpi;
 
  for(int i=1; i<(n>>2); i++)
  {
    i2=1+(i1=i+i);
    i4=1+(i3=n-i1);
    h1r=c1*(realData[i1]+realData[i3]);
    h1i=c1*(realData[i2]-realData[i4]);
    h2r=-c2*(realData[i2]+realData[i4]);
    h2i=c2*(realData[i1]-realData[i3]);
    
    realData[i1] = h1r+wr*h2r-wi*h2i;
    realData[i2] = h1i+wr*h2i+wi*h2r;
    realData[i3] = h1r-wr*h2r+wi*h2i;
    realData[i4] = -h1i+wr*h2i+wi*h2r;
    
    wr = (wtemp=wr)*wpr-wi*wpi+wr;
    wi = wi*wpr+wtemp*wpi+wi; 
    
  }

  realData[0] = (h1r=realData[0])+realData[1];
  realData[1] = h1r-realData[1];
  
  
}

bool is_power_of_2(size_t n) {
    return n>0 && !(n&(n-1));
}

/////////////////////////////////////////////////////////////////////

bool VoicePitchCalculator::onProcessSamples(const sf::Int16* samples, std::size_t sampleCount)
{
 
  //Support Variables
  int j=0;
  
  //refresh the samplecount of the class every time onProcessSamples is called
  m_SampleCount=sampleCount;
 
  //resize the vector of fourier components
  m_FourierComponents.resize(m_SampleCount);
    
  //fill it with the samples
  for (int i=0; i<m_SampleCount; ++i)
  {
    m_FourierComponents[i]=samples[i];
  
  }
  //control if sampleCount is a power of 2
  if (!is_power_of_2(m_SampleCount)) 
  {
    //if it isn't, fill with zeros until it is.
    fillZeros(m_FourierComponents,&m_SampleCount);  
  }
   
//Compute fft
  RealFFT(m_FourierComponents);
   
  //multiply every component with the step in frequency // is this really useful?
/*  for (int i=0; i<m_FourierComponents.size(); ++i)
    {
      m_FourierComponents[i]*=float(m_SampleCount)/44100;
    //std::cout << i << " : " << m_FourierComponents[i] << std::endl;
    }*/

  //assign the max
  m_Max=std::fmax(*std::max_element(m_FourierComponents.begin(),m_FourierComponents.end()),std::abs(*std::min_element(m_FourierComponents.begin(),m_FourierComponents.end())));
  
  if (*std::max_element(m_FourierComponents.begin(),m_FourierComponents.end()) > std::abs(*std::min_element(m_FourierComponents.begin(),m_FourierComponents.end())))
    m_Pitch=(std::max_element(m_FourierComponents.begin(),m_FourierComponents.end())-m_FourierComponents.begin());
  else m_Pitch=(std::min_element(m_FourierComponents.begin(),m_FourierComponents.end()) -m_FourierComponents.begin());
  
  
  //fill bars vector
  //resize

  std::unique_lock<std::mutex> lock(m_mutex);
  m_stringPitch = std::to_string(float(m_Pitch*44100)/(2*float(m_SampleCount)));
  m_appo=sampleCount;
  m_Bars.resize(sampleCount/2);
  
    for (j=0; j< ( sampleCount/2 ); ++j)
        {
          m_Bars[j].setSize(sf::Vector2f(log10(2800.f/float(sampleCount/2)),(pow(m_FourierComponents[j],2)/float(pow(m_Max,2)))*800.f));
          if (j<20 || j>20000) m_Bars[j].setFillColor(sf::Color::Black);
          else  
          { 
            m_Color.SetSoundFreq(float(j)*44100/(2*float(m_SampleCount))); 
            m_Bars[j].setFillColor(m_Color.m_Color); 
          }
          m_Bars[j].setOrigin(0,m_Bars[j].getSize().y);
          m_Bars[j].setPosition(j*2800.f/float(sampleCount/2),900);

        m_free=true;
      
        }
       
  m_readyToDraw.notify_one();
     
  return true;
  
}

void VoicePitchCalculator::ShowSpectrum(sf::RenderWindow &SpectrumWindow, sf::Event &Event, tgui::Gui  &mainGui, sf::Font &font, tgui::Theme::Ptr theme,mode &Mode)
{
  sf::Text message("You are singing at ",font,75);
  sf::Text freq("440 Hz",font,75);
  sf::RectangleShape               Indicator;
  
  
  
//buttons
  tgui::Button::Ptr Start = theme->load("Button");
  tgui::Button::Ptr Stop = theme->load("Button");
  tgui::Button::Ptr Back = theme->load("Button"); 
  mainGui.add(Start);
  mainGui.add(Stop);
  mainGui.add(Back);

//back button
  Back->setSize(SpectrumWindow.getSize().x*0.2,SpectrumWindow.getSize().y*0.1);
  Back->setPosition(SpectrumWindow.getSize().x/2-Back->getSize().x/2,SpectrumWindow.getSize().y*0.8);
  Back->setFont(font);
  Back->setTextSize(75);
  Back->setText("Back");
  Back->connect("pressed",[&](){stop();
                                Mode=menu;});
  Back->show();
  
//Start
  Start->setSize(SpectrumWindow.getSize().x*0.2,SpectrumWindow.getSize().y*0.1);
  Start->setPosition(SpectrumWindow.getSize().x/2+SpectrumWindow.getSize().x*0.25-Start->getSize().x/2,SpectrumWindow.getSize().y*0.8);
  Start->setFont(font);
  Start->setTextSize(75);
  Start->setText("Start");
  Start->connect("pressed",[&](){start();});
  Start->show();

//Stop
  Stop->setSize(SpectrumWindow.getSize().x*0.2,SpectrumWindow.getSize().y*0.1);
  Stop->setPosition(SpectrumWindow.getSize().x/2-SpectrumWindow.getSize().x*0.25-Stop->getSize().x/2,SpectrumWindow.getSize().y*0.8);
  Stop->setFont(font);
  Stop->setTextSize(75);
  Stop->setText("Stop");
  Stop->connect("pressed",[&](){stop();
                                m_free=false;});
  Stop->show();  

//text
  message.setFillColor(sf::Color::Black);
  message.setOrigin(message.getGlobalBounds().width/2+message.getGlobalBounds().left,0);
  message.setPosition(SpectrumWindow.getSize().x/2,Stop->getPosition().y-SpectrumWindow.getSize().y*0.15);
  
  freq.setFillColor(sf::Color::Black);
  freq.setOrigin(freq.getGlobalBounds().width/2+freq.getGlobalBounds().left,0);
  freq.setPosition(message.getPosition().x+message.getGlobalBounds().width,Stop->getPosition().y-SpectrumWindow.getSize().y*0.15);
  
  while (SpectrumWindow.isOpen())
  {
    if (Mode==menu) 
    { 
      Start->hide();
      Stop->hide();
      Back->hide();
      break;
    }
    
    SpectrumWindow.clear(sf::Color::White); 
    
    while(SpectrumWindow.pollEvent(Event))
    {
      mainGui.handleEvent(Event);
      if (Event.type == sf::Event::Closed)  
      {
        SpectrumWindow.close();
        stop();
      }  
    }
    
      Indicator.setSize(sf::Vector2f(100,100));
      Indicator.setPosition(message.getPosition().x-message.getGlobalBounds().width,Stop->getPosition().y-SpectrumWindow.getSize().y*0.15);
      
      
        //resize each bar size, position and color.
       if(m_free) {
      std::unique_lock<std::mutex> lock(m_mutex); 
      m_Color.SetSoundFreq(float(m_Pitch*44100)/(2*float(m_SampleCount)));
      Indicator.setFillColor(m_Color.m_Color);

      for (int i=0; i<(m_appo/2); ++i)
        {         
          SpectrumWindow.draw(m_Bars[i]);    
             
        }
      freq.setString(m_stringPitch+" Hz");  
      SpectrumWindow.draw(freq); 
      SpectrumWindow.draw(Indicator);
        
        m_free=true;       
        }
       
    m_readyToDraw.notify_one();	  
    SpectrumWindow.draw(message);    
    mainGui.draw();
    SpectrumWindow.display();
  }
} 



#endif