
CXXFLAGS = -std=c++11 
LDFLAGS = -lsfml-graphics  -lsfml-window -lsfml-system -lsfml-audio -ltgui


main: main.o SoundRGB.o SoundColor.o SfmlSoundColor.o PolygonalPiano.o VoicePitchCalculator.o;\
  $(LINK.cc) $^ -o $@ 

main.o: main.cpp;\
  $(COMPILE.cc) $^ 
  
VoicePitchCalculator.o: VoicePitchCalculator.cpp VoicePitchCalculator.hpp;\
  $(COMPILE.cc) $^
  
PolygonalPiano.o: PolygonalPiano.cpp PolygonalPiano.hpp;\
  $(COMPILE.cc) $^

SfmlSoundColor.o: SfmlSoundColor.cpp SfmlSoundColor.hpp;\
  $(COMPILE.cc) $^ 

SoundRGB.o: SoundRGB.cpp SoundRGB.hpp;\
  $(COMPILE.cc) $^

SoundColor.o: SoundColor.cpp SoundColor.hpp;\
  $(COMPILE.cc) $^ 
  

    
    
clean: ;\
  $(RM) *.o  