
#ifndef POLYGONALPIANO_CPP
#define POLYGONALPIANO_CPP

#include "PolygonalPiano.hpp"

int PianoKey::GetKeyNumber()
{
  if (m_octave == 0)
  {
    return static_cast<int>(m_note - 12);
  }
  else
  {
    return (12*(m_octave-1)+static_cast<int>(m_note));
  }
}


// The formula for the frequency in a piano where the number 1 is the//
// A0 is: f=2^((n-49)/12)*440 Hz , but here the A0 is the -2 key, so //
// in the formula there is a +3 in the exponent.                     //

float PianoKey::GetSoundFreq()
{
  return pow(2,((GetKeyNumber()-46.f)/12.f))*440.f;
}
////////////////////////////////////////////////
  
void PolygonalPiano::m_GeneratePiano()
{ 
  //Support variable
  PianoKey n_Key;
  
  //The first note
  n_Key.SetOctave(0);
  n_Key.SetNote(A);
   
  //Add the 0th octave keys
  m_Keyboard.push_back(n_Key);
  n_Key.SetNote(Bb);
  m_Keyboard.push_back(n_Key);
  n_Key.SetNote(B);
  m_Keyboard.push_back(n_Key);
   

  //Add the 7 full octaves keys 
  for (int i=0; i<7; ++i)
  {
    n_Key.SetOctave(i+1);
    for (int j=0; j<12; ++j)
    {
      n_Key.SetNote(static_cast<note>(j+1));
      m_Keyboard.push_back(n_Key);
    }
  }
   
  //Add the last key
  n_Key.SetOctave(8);
  n_Key.SetNote(C);
  m_Keyboard.push_back(n_Key);
}


void PolygonalPiano::Play(sf::RenderWindow &PianoWindow, sf::Event &Event, tgui::Gui  &mainGui, sf::Font &font, tgui::Theme::Ptr theme,mode &Mode)  
{
 
  //Support variables
  std::string      NotesNames[12] = {"C","Db","D","Eb","E","F","Gb","G","Ab","A","Bb","B"};
  PianoKey         CurrentNote(4,C); //The default octave chosen is the 4th.
  bool             OctaveIsChanged(true),showInfo(false);
  OctaveCases      OCase(Other); 
 
  int              KeyboardKeys[12] = {0,22,18,4,3,5,19,6,24,7,20,9};
  int              Counter=0;
  SfmlSoundColor   Color(440.f);
  sf::Text         textOctave,textNote,info,info1;
  std::string      stringNote="You are playing a ",stringOctave="Octave number: ";
  
  //piano bottons
  tgui::Button::Ptr Plus = theme->load("Button");
  tgui::Button::Ptr Minus = theme->load("Button");
  tgui::Button::Ptr Back = theme->load("Button"); 
  tgui::Button::Ptr Info = theme->load("Button"); 
  mainGui.add(Plus);
  mainGui.add(Minus);
  mainGui.add(Back);
  mainGui.add(Info);
  
  
  
  //text settings
  
  
  //info
  info.setFillColor(sf::Color::Black);
  info.setFont(font);
  info.setCharacterSize(50);
  info.setString("Use the keys AWSEDFTGYHUJ to play. Every octave goes from C to B,");
  info.setOrigin(info.getGlobalBounds().width/2+info.getGlobalBounds().left,0);
  
  //info1
  info1.setFillColor(sf::Color::Black);
  info1.setFont(font);
  info1.setCharacterSize(50);
  info1.setString("exception made for the first and last ones. Enjoy it!");
  info1.setOrigin(info1.getGlobalBounds().width/2+info1.getGlobalBounds().left,0);
  
  
  //octave
  textOctave.setFillColor(sf::Color::Black);
  textOctave.setFont(font);
  textOctave.setCharacterSize(75);
  textOctave.setString(stringOctave+std::to_string(CurrentNote.GetOctave()));
  textOctave.setOrigin(textOctave.getGlobalBounds().width/2+textOctave.getGlobalBounds().left,0);
  
  //note
  textNote.setFillColor(sf::Color::Black);
  textNote.setFont(font);
  textNote.setCharacterSize(75);
  textNote.setString(stringNote+NotesNames[0]);
  textNote.setOrigin(textNote.getGlobalBounds().width/2,0);
  
//BUttons 

  //Plus
  Plus->setSize(PianoWindow.getSize().x*0.2,PianoWindow.getSize().y*0.1);
  Plus->setPosition(PianoWindow.getSize().x/2+PianoWindow.getSize().x*0.25-Plus->getSize().x/2,PianoWindow.getSize().y*0.8);
  Plus->setFont(font);
  Plus->setTextSize(75);
  Plus->setText("Next octave");
  Plus->connect("pressed",[&](){if (CurrentNote.GetOctave()==8) 
                              {textOctave.setString("You've reached the highest octave.");}
                              else 
                              {CurrentNote.SetOctave(CurrentNote.GetOctave()+1);
                              OctaveIsChanged=true;}});
  Plus->show();

  //Minus
  Minus->setSize(PianoWindow.getSize().x*0.2,PianoWindow.getSize().y*0.1);
  Minus->setPosition(PianoWindow.getSize().x/2-PianoWindow.getSize().x*0.25-Minus->getSize().x/2,PianoWindow.getSize().y*0.8);
  Minus->setFont(font);
  Minus->setTextSize(75);
  Minus->setText("Prev. octave");
  Minus->connect("pressed",[&](){if (CurrentNote.GetOctave()==0) 
                               {textOctave.setString("You've reached the lowest octave.");}
                               else 
                               {CurrentNote.SetOctave(CurrentNote.GetOctave()-1);
                               OctaveIsChanged=true;}});
  Minus->show();

  //Back
  Back->setSize(PianoWindow.getSize().x*0.2,PianoWindow.getSize().y*0.1);
  Back->setPosition(PianoWindow.getSize().x/2-Back->getSize().x/2,PianoWindow.getSize().y*0.8);
  Back->setFont(font);
  Back->setTextSize(75);
  Back->setText("Back");
  Back->connect("pressed",[&](){Mode=menu;});
  Back->show();
  
  //Info
  Info->setSize(PianoWindow.getSize().y*0.05,PianoWindow.getSize().y*0.05);
  Info->setPosition(PianoWindow.getSize().x/2-Info->getSize().x/2,PianoWindow.getSize().y*0.03);
  Info->setFont(font);
  Info->setTextSize(60);
  Info->setText("i");
  Info->connect("pressed",[&](){if(!showInfo)showInfo=true;
                                else showInfo=false; });
  Info->show();
  
  
  info.setPosition(PianoWindow.getSize().x/2,Back->getPosition().y-PianoWindow.getSize().y*0.15);
  info1.setPosition(PianoWindow.getSize().x/2,info.getPosition().y+PianoWindow.getSize().y*0.05);
   
  m_OctaveArray.resize(12); //So that it can contain an octave.


        
  while (PianoWindow.isOpen())
  {
    if (Mode==menu) 
    { 
      Plus->hide();
      Minus->hide();
      Back->hide();
      Info->hide();
      
      break;
    }
    
    //Fill the buffers array with the current octave (only if the octave is changed)  
    //and assign the sounds to the right buffers. this implementation loads one file
    //for each cycle, until the array is all loaded. In this way the loading delay is 
    //shorter and less evident. 
     
    if (OctaveIsChanged)
    { 
      textOctave.setString(stringOctave+std::to_string(CurrentNote.GetOctave()));
      
      if (CurrentNote.GetOctave() == 0)
      {
        OCase=First;
        if ((Counter+9)<12)
        {
        if (!m_OctaveArray[Counter+9].loadFromFile("Samples/Piano.ff."+NotesNames[Counter+9]+std::to_string(CurrentNote.GetOctave())+".wav"))
            {
              //debug
              std::cout << "errore nel caricamento dei file" ;
            }
        m_Keyboard[Counter+9+12*(CurrentNote.GetOctave()-1)+3].KeySound.setBuffer(m_OctaveArray[Counter+9]);
        ++Counter;    
        } else 
        {
          OctaveIsChanged=false;
          Counter=0;  
        }
      }
      else if (CurrentNote.GetOctave()<8)
      { 
        OCase=Other;
        if (Counter<12)
        {
        if (!m_OctaveArray[Counter].loadFromFile("Samples/Piano.ff."+NotesNames[Counter]+std::to_string(CurrentNote.GetOctave())+".wav"))
            {
              //debug
              std::cout << "errore nel caricamento dei file" ;
            }
        m_Keyboard[Counter+12*(CurrentNote.GetOctave()-1)+3].KeySound.setBuffer(m_OctaveArray[Counter]);
        ++Counter;    
        } else 
        {
          OctaveIsChanged=false;
          Counter=0;  
        }
      }
      else
      {
        OCase=Last;
        if (!m_OctaveArray[0].loadFromFile("Samples/Piano.ff."+NotesNames[0]+std::to_string(CurrentNote.GetOctave())+".wav"))
            {
              //debug
              std::cout << "errore nel caricamento dei file" ;
            }
        m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].KeySound.setBuffer(m_OctaveArray[0]);
        OctaveIsChanged=false;
        Counter=0;
      }
    }
   

    PianoWindow.clear(sf::Color::White);  
  //Interpret the keyboard input and play the requested sounds    
  if (OCase == Other) //one of the full octaves
  {
   for (int i=0; i<12; ++i)
   {
    if (sf::Keyboard::isKeyPressed((sf::Keyboard::Key)KeyboardKeys[i]))
      {
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setPointCount(14-i);
       Color.SetSoundFreq(m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].GetSoundFreq());
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setFillColor(Color.m_Color);
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setRadius(100);
       
       if (i==1 || i==3 || i==6 || i==8 || i==10)
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setPosition(200*i,PianoWindow.getSize().y*0.35);
       else
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setPosition(200*i,PianoWindow.getSize().y*0.5);
       
       textNote.setString(stringNote+NotesNames[i]);
       textNote.setPosition(PianoWindow.getSize().x/2,PianoWindow.getSize().y*0.15);
  
       PianoWindow.draw(m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon);
       PianoWindow.draw(textNote);
       
       if (m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].KeySound.getStatus()==sf::Sound::Stopped) 
       {
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].KeySound.play();
       }
      } else {m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].KeySound.stop();}
   }
  }
  else if (OCase == First)
  {
    for (int i=9; i<12; ++i)
   {
    if (sf::Keyboard::isKeyPressed((sf::Keyboard::Key)KeyboardKeys[i]))
      {
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setPointCount(14-i);
       Color.SetSoundFreq(m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].GetSoundFreq());
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setFillColor(Color.m_Color);
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setRadius(100);
       
       if (i==10)
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setPosition(200*i,PianoWindow.getSize().y*0.35);
       else
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon.setPosition(200*i,PianoWindow.getSize().y*0.5);
       
       textNote.setString(stringNote+NotesNames[i]);
       textNote.setPosition(PianoWindow.getSize().x/2,PianoWindow.getSize().y*0.15);
  
       PianoWindow.draw(m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].Polygon);
       PianoWindow.draw(textNote);
       
       if (m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].KeySound.getStatus()==sf::Sound::Stopped) 
       {
       m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].KeySound.play();
       }
      } else {m_Keyboard[i+12*(CurrentNote.GetOctave()-1)+3].KeySound.stop();}
   }
  }
  else if (OCase == Last)
  {
    
    if (sf::Keyboard::isKeyPressed((sf::Keyboard::Key)KeyboardKeys[0]))
      {
       m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].Polygon.setPointCount(14);
       Color.SetSoundFreq(m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].GetSoundFreq());
       m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].Polygon.setFillColor(Color.m_Color);
       m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].Polygon.setRadius(100);
       
       m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].Polygon.setPosition(0,PianoWindow.getSize().y*0.5);
       
       textNote.setString(stringNote+NotesNames[0]);
       textNote.setPosition(PianoWindow.getSize().x/2,PianoWindow.getSize().y*0.15);
  
       PianoWindow.draw(m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].Polygon);
       PianoWindow.draw(textNote);
       
       if (m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].KeySound.getStatus()==sf::Sound::Stopped) 
       {
       m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].KeySound.play();
       }
      } else {m_Keyboard[12*(CurrentNote.GetOctave()-1)+3].KeySound.stop();}
   
  }
      
   //Event loop  
       while (PianoWindow.pollEvent(Event))
    { 
      mainGui.handleEvent(Event);
      if(Event.type == sf::Event::Closed) PianoWindow.close();      
    }

    
    textOctave.setPosition(PianoWindow.getSize().x/2,PianoWindow.getSize().y*0.1);
    PianoWindow.draw(textOctave);
    if (showInfo)
    {
      PianoWindow.draw(info);
      PianoWindow.draw(info1);
    }
    mainGui.draw(); 
    PianoWindow.display();     
  }
  
}

#endif
