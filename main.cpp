///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////

#include <TGUI/TGUI.hpp>
#include "PolygonalPiano.hpp"
#include "VoicePitchCalculator.hpp"


int main() 
{

sf::ContextSettings settings;
settings.antialiasingLevel = 30;
sf::VideoMode desktop = sf::VideoMode::getDesktopMode();

sf::RenderWindow  mainWindow(desktop,"Synesthesia",sf::Style::Default,settings);
tgui::Gui         mainGui(mainWindow);
sf::Event         event;
mode              Mode=begin;


PolygonalPiano        MyPiano;
VoicePitchCalculator  MyVoice;

// Declaration of fonts used
sf::Font font;
font.loadFromFile("Drugs.otf");

// declaration of texts:

// begin title and subtitles

sf::Text Title("Synesthesia",font,375);
sf::Text Subtitle("Who said you could not see sounds?",font,100);

Title.setFillColor(sf::Color::Red);
Title.setOrigin(Title.getGlobalBounds().width/2+Title.getGlobalBounds().left,0);
Title.setPosition(mainWindow.getSize().x/2,mainWindow.getSize().y*0.12);
Title.setStyle(sf::Text::Bold);

Subtitle.setFillColor(sf::Color::White);
Subtitle.setOrigin(Subtitle.getGlobalBounds().width/2+Subtitle.getGlobalBounds().left,0);
Subtitle.setPosition(mainWindow.getSize().x/2,Title.getPosition().y+Title.getGlobalBounds().height+mainWindow.getSize().y*0.10);

// menu text

sf::Text Introduction("Hello, this is Synesthesia. It allows you to see sounds. Yes, you can \nliterally see sounds. But don't waste our time, let's try! If you want \nto play a piano that shows a different colored regular polygon \nfor each note you play in a octave, click on Polygonal Piano. If you \nwant to see the color of your voice and to know which note you \nare singing, click on Voice Pitch Calculator.",font, 75);
Introduction.setFillColor(sf::Color::White);
Introduction.setOrigin(Introduction.getGlobalBounds().width/2+Introduction.getGlobalBounds().left,0);
Introduction.setPosition(mainWindow.getSize().x/2,75);

//Gui instances

tgui::Theme::Ptr theme = std::make_shared<tgui::Theme>("Black.txt");

//begin buttons

tgui::Button::Ptr Enter = theme->load("Button");
mainGui.add(Enter);
tgui::Button::Ptr Quit = theme->load("Button");
mainGui.add(Quit);

//menu buttons

tgui::Button::Ptr Piano = theme->load("Button");
tgui::Button::Ptr Voice = theme->load("Button");
tgui::Button::Ptr Close = theme->load("Button");
mainGui.add(Piano);
mainGui.add(Voice);
mainGui.add(Close);



// Buttons instances and characteristics

//Enter

Enter->setSize(mainWindow.getSize().x*0.2,mainWindow.getSize().y*0.1);
Enter->setPosition(mainWindow.getSize().x/2-Enter->getSize().x/2,Subtitle.getPosition().y+Subtitle.getGlobalBounds().height+mainWindow.getSize().y*0.12);
Enter->setFont(font);
Enter->setText("Enter");
Enter->connect("pressed",[&](){Mode=menu;});
Enter->hide();

//Quit

Quit->setSize(mainWindow.getSize().x*0.2,mainWindow.getSize().y*0.1);
Quit->setPosition(mainWindow.getSize().x/2-Quit->getSize().x/2,Enter->getPosition().y+mainWindow.getSize().y*0.12);
Quit->setFont(font);
Quit->setText("Quit");
Quit->connect("pressed",[&](){Mode=menu;});
Quit->hide();

//Piano
Piano->setSize(mainWindow.getSize().x*0.25,mainWindow.getSize().y*0.1);
Piano->setPosition(mainWindow.getSize().x/2-mainWindow.getSize().x*0.25-Piano->getSize().x/2,Introduction.getPosition().y+Introduction.getGlobalBounds().height+mainWindow.getSize().y*0.2);
Piano->setFont(font);
Piano->setTextSize(75);
Piano->setText("Polygonal Piano");
Piano->connect("pressed",[&](){Mode=piano;});
Piano->hide();

//Voice
Voice->setSize(mainWindow.getSize().x*0.35,mainWindow.getSize().y*0.1);
Voice->setPosition(mainWindow.getSize().x/2+mainWindow.getSize().x*0.25-Voice->getSize().x/2,Introduction.getPosition().y+Introduction.getGlobalBounds().height+mainWindow.getSize().y*0.2);
Voice->setFont(font);
Voice->setTextSize(75);
Voice->setText("Voice Pitch Calculator");
Voice->connect("pressed",[&](){Mode=voice;});
Voice->hide();

//Close
Close->setSize(mainWindow.getSize().x*0.25,mainWindow.getSize().y*0.1);
Close->setPosition(mainWindow.getSize().x/2-Close->getSize().x/2,Introduction.getPosition().y+Introduction.getGlobalBounds().height+mainWindow.getSize().y*0.4);
Close->setFont(font);
Close->setTextSize(75);
Close->setText("Quit");
Close->connect("pressed",[&](){mainWindow.close();});
Close->hide();



//
// The mainWindow loop

while (mainWindow.isOpen())
{
  switch (Mode)
  {
////////////////////////////////// Begin page ////////////////////////////////////////////
  
    case begin:
      
     
      Enter->show();
      Quit->show();
      mainWindow.clear(sf::Color::Black);
      mainWindow.draw(Title);
      mainWindow.draw(Subtitle);



    break;
////////////////////////////////// Main menù /////////////////////////////////////////////

    case menu:
      
      // hide unwanted widgets
      Enter->hide();
      Quit->hide();
      
      // show wanted widgets
      Piano->show();
      Voice->show();
      Close->show();
      
      
      mainWindow.clear(sf::Color::Black);
      mainWindow.draw(Introduction);
    break;

////////////////////////////////// PolPiano //////////////////////////////////////////////

    case piano:
      // hide unwanted widgets
      Piano->hide();
      Voice->hide();
      Close->hide();
 
      MyPiano.Play(mainWindow,event,mainGui,font,theme,Mode);
      

    break;

////////////////////////////////// VoicePitch ////////////////////////////////////////////
    
    case voice:
      // hide unwanted widgets
      Piano->hide();
      Voice->hide();
      Close->hide();
      
      MyVoice.ShowSpectrum(mainWindow,event,mainGui,font,theme,Mode);
      
    break;
}
//////////////////////////////////////////////////////////////////////////////////////////
  // The events loop
  
  while(mainWindow.pollEvent(event))
  { 
    mainGui.handleEvent(event);   
    // close the window if requested
    if (event.type == sf::Event::Closed) mainWindow.close();
    
    // if the window is resized
   // if (event.type == sf::Event::Resized) 
  //    mainWindow.setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
  
  }
  
  
  mainGui.draw();
  mainWindow.display();

}



return 0;
}