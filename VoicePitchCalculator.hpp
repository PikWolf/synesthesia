///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////



#ifndef VOICEPITHCHCALCULATOR_HPP
#define VOICEPITHCHCALCULATOR_HPP

#include <SFML/Audio.hpp>
#include <TGUI/TGui.hpp>
#include <vector>
#include "SfmlSoundColor.hpp"
#include <mutex>
#include <utility>
#include <cmath>
#include <condition_variable>

class VoicePitchCalculator : public sf::SoundRecorder
{
  
  public:
  
//No specific constructor 

// The principal and most important method of the class. It contains //
// the window loop and the code that records the input, makes the fft//
// and show the animation. It also processes the gui.                // 
// All these arguments are needed in order to manage the window and  //
// the gui of the program that calls this method                     //
  
  void ShowSpectrum(sf::RenderWindow &SpectrumWindow, sf::Event &Event, tgui::Gui  &mainGui, sf::Font &font, tgui::Theme::Ptr theme,mode &Mode);
  
  private:
  
// The shape that are drawn in the window to represent the spectrum

// This vector contains the rectangles that will be drawn to on the  //
// window to represent the fft.                                      //

  std::vector<sf::RectangleShape>  m_Bars;

// This vector contains the samples recorded that, after the fft, are//
// replaced by the frequency components                              //

  std::vector<double>              m_FourierComponents;

// The colored rectangle that is shown while recording               //
  sf::RectangleShape               m_Indicator;

// All these members are support variables that are important for the//
// methods of the class and are declared here because they need to be//
// accessible by two different methods: ShowSpectrum and             //
// onProcessSamples.                                                 //

  size_t                           m_SampleCount;
  float                            m_Max;
  float                            m_Pitch;
  SfmlSoundColor                   m_Color; 
  std::mutex                       m_mutex;
  bool                             m_free =false;
  int                              m_appo;
  std::condition_variable          m_readyToDraw;
  std::string                      m_stringPitch;
  
// implementation of the virtual methods of the mother class         //
// sf::SoundRecorder                                                 //

// called when the acquisition starts                                //
  
  virtual bool onStart()  {setProcessingInterval(sf::milliseconds(40));	 return true;}

// the method that processes the chunks of samples recorded          //
  virtual bool onProcessSamples(const sf::Int16* samples, std::size_t sampleCount);
  
  

};


#endif