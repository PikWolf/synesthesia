///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////

//This class represent the object actually used in the project: a    //
//SoundColor that Sfml can manage.                                   //


#ifndef SFMLSOUNDCOLOR_HPP
#define SFMLSOUNDCOLOR_HPP

#include <SFML/Graphics.hpp>
#include "SoundRGB.hpp"

class SfmlSoundColor : public SoundRGB 
{
  public:
  
//Constructor

// Constructor that assigns the frequency of the object              //
//argument:                                                          //
//SoundFreq: the sound frequency of the SfmlSoundColor object        //

  SfmlSoundColor(float SoundFreq);
  
// Default constructor                                               //
  
  SfmlSoundColor() {}

//Set methods

// Set method that implements the virtual method of the mother class //

    void SetSoundFreq(float SoundFreq);  

// The most important feature of this class: a sf::Color. It is made //
// to make it easy to use the methods of the sf::Color class.        //
    sf::Color m_Color;
     
};



#endif