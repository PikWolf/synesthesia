//programma di prova per sperimentare le classi create

#include <iostream>
#include "SoundColor.hpp"
#include "SoundRGB.hpp"
#include "SfmlSoundColor.hpp" 
#include "PolygonalPiano.hpp"
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include "VoicePitchCalculator.hpp"


/////////Class////////////
class MyStream : public sf::SoundStream 
{
  public:
  
  float SoundFreq;
  
  MyStream(float Freq)
  {
  initialize(1,44100);
  SoundFreq=Freq;
  m_current_sample=0;
  }


  private:
  
  virtual bool onGetData(Chunk& data)
  {
    sf::Lock lock(m_mutex);
    const int SamplesPerChunk = getSampleRate() * getChannelCount() *1 ; 
    const double pi = 3.141592653589793238463; //per adesso è qua, ma provvisorio
    double x=0;
    
    
    for (int i=0; i < SamplesPerChunk; ++i)
    {
      
      Chunks.push_back(10000*sin(x*(2*pi)));
      x+= SoundFreq/44100;
    }
    
    data.samples=&Chunks[m_current_sample];
    data.sampleCount=SamplesPerChunk;
    m_current_sample += SamplesPerChunk;
    return true;
  }
  
   virtual void onSeek(sf::Time timeOffset)
  {
     m_current_sample = static_cast<std::size_t>(timeOffset.asSeconds() * getSampleRate() * getChannelCount());
  }
  
  std::vector<sf::Int16> Chunks;
  std::size_t m_current_sample;
  
  sf::Mutex              m_mutex;
  
};
//////////////////////////////

int main() {

  //sintetizzatore
/*{  sf::RenderWindow window(sf::VideoMode(1200,900),"prova");
  sf::SoundBuffer buffer;

  long int i=0;
  float freq=440.f;
  float freq1=10000.f;
  float Pitch=1;
  const double pi = 3.1415926535897;
  std::vector<sf::Int16> samples;
  SfmlSoundColor Color(freq);
  SfmlSoundColor Color1(freq1);
  SfmlSoundColor Sum;
  

// Main loop

  MyStream sintetizzatore(freq);
  MyStream sintetizzatore1(freq1);

  
  sintetizzatore.play();
  //sintetizzatore1.play();
   
  sf::Event event;
  while (window.isOpen())
  {

  
   
   while ((sintetizzatore.getStatus() == MyStream::Playing) && (sintetizzatore1.getStatus() == MyStream::Playing))
   {
        //sf::sleep(sf::seconds(0.1f));
              
    while (window.pollEvent(event))
      {
    
    if(event.type == sf::Event::Closed)
    {
      window.close();   
      sintetizzatore.stop();
      sintetizzatore1.stop();
    }
    else if  (event.type == sf::Event::TextEntered)
    {
      if (event.text.unicode < 128)
      {
        if      ((event.text.unicode) == '+')
          { 
            freq=freq+10; 
            freq1=freq1+10; 
            Color.SetSoundFreq(freq);
            sintetizzatore.SoundFreq=freq;
            Color1.SetSoundFreq(freq1);
            sintetizzatore1.SoundFreq=freq1;
          }
        else if ((event.text.unicode) == '-') 
          {
            freq=freq-10;
            Color.SetSoundFreq(freq);
            sintetizzatore.SoundFreq=freq;
            freq1=freq1-10;
            Color1.SetSoundFreq(freq1);
            sintetizzatore1.SoundFreq=freq1;
            
          }
        else if (((event.text.unicode) == 'p') or ((event.text.unicode) == 'P'))
          {
            sintetizzatore.pause();
            sintetizzatore1.pause();
          }
        else if (((event.text.unicode) == 'g') or ((event.text.unicode) == 'G'))
          {
            sintetizzatore.play();
            sintetizzatore1.play();
          }  
      }
      
    } 
    
      
    //std::cout << static_cast<char>(event.text.unicode) << std::endl;  
    std::cout << freq << std::endl; 
    std::cout << freq1 << std::endl; 
    //std::cout << static_cast<int>(Color.m_Color.r) << " " << static_cast<int>(Color.m_Color.g) << " " << static_cast<int>(Color.m_Color.b) << std::endl; 
    }
    
////////////////
     
    window.clear(Color.m_Color);
    
    window.display();
    }
  }
  }*/ 

//prova del piano poligonale 

PolygonalPiano Steinway;
int a;



Steinway.Play();


//prova del registratore

/*int a;

if (sf::SoundRecorder::isAvailable())
{
    // Record some audio data
    VoicePitchCalculator recorder;
    std::cout << "schiaccia un tasto per registrare, lascialo per smettere" << std::endl;
    recorder.start();
    recorder.ShowSpectrum();  
   // if (std::cin >> a) recorder.stop();
 
}
else
{
std::cout << "Erroree" ;
}*/

return 0;
}













   