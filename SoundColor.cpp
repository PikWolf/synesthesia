#ifndef SOUNDCOLOR_CPP
#define SOUNDCOLOR_CPP

#include "SoundColor.hpp"



//funzione che associa, in maniera "continua" il valore 
//di lunghezza d'onda della luce al valore di frequenza del suono
//i valori numerici utilizzati per i fondi scala della luce: 360(violetto) e 750(rosso)
//sono da intendersi espressi in nm, ma ciò non è rilevante ai fini del programma.

  float SoundColor::ligthWLFromSoundFreq(float SoundFreq)
  {
    float a,b;
  
    a=-360.f/log10(20000/20); //cambio il range
    b=750.f-a*log10(20);
    return (a*log10(SoundFreq)+b);
  }


#endif