///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////


// This is the mother class of the project Synesthesia.              //
// It contains the fundamental object of the entire project:         //
// the SoundColor                                                    // 

#ifndef SOUNDCOLOR_HPP
#define SOUNDCOLOR_HPP

#include <cmath>

enum              mode {begin,menu,piano,voice};

 
class SoundColor {  

public: 

// Constructor

// Explicit constructor                                              //
// argument:                                                         //
// freq: the sound frequency of the SoundColor object                //

  SoundColor (float freq) :  m_freq(freq) {} 

// Default constructor                                               //

  SoundColor () {}
  
// Access methods

// Get method that returns the wavelength of the SoundColor object   //
// No arguments                                                      //

  float GetColorWL() {return ligthWLFromSoundFreq(m_freq);}

// Get method that returns the sound frequency of the soundcolor obj.//
// No arguments                                                      //

  float GetFreq() {return m_freq;};
   
// Set methods

// Set method that allows one to set the sound frequency of the      //
// SoundColor object                                                 //
// argument:                                                         //
// freq: the sound frequency of the SoundColor object                //    
// This method must be implemented in every daughter class           //                                                        
  
  virtual void SetSoundFreq(float freq) {m_freq=freq;}
  

  
protected: 

// Protected methods

// This method returns a numerical value representing the wavelength //
// of the SoundColor object (in nm units) given a sound frequency    //
// value. It spans the range [400 , 700]                             //
// argument:                                                         // 
// SoundFreq: the sound frequency of the SoundColor object. It must  // 
// be between 20 and 20000                                           //

  float ligthWLFromSoundFreq(float SoundFreq);

// Protected members  

// The sound frequency of the SoundColor object is the only member   //
// needed to characterize it. The wavelength is function of this     //
// parameter                                                         // 

  float m_freq;
   

};





#endif
