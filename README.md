
Synestesia is a software that has at its core a mapping between the wavelength of visible radiation and the frequency of audible sound.
This allows to "see" sounds or to "hear" colors.
On top of that, two user interfaces are built: 

### Polygonal Piano

Using the keyboard, the user can play piano sounds, and on the screen flat polygons appear. Each note has 
a different polygon. 

### Voice Pitch Calculator

It's FFT based spectrum visualizer that also display the note currently sang by the user.