///// Created by Andrea Pogliani /////
///// apoglia@gmail.com //////////////


// Inherits from SoundColor class. It is a SoundColor that has also  //
// information about the RGBA components of the color associated to  //
// the Sound                                                         // 

#ifndef SOUNDRGB_HPP
#define SOUNDRGB_HPP


#include "SoundColor.hpp"
#include <cmath>

class SoundRGB: public SoundColor 

{ 

public:

//constructor

// Default constructor                                               //

  SoundRGB () {}

// Explicit constructor                                              //
// argument:                                                         //
// SoundFreq: the sound frequency of the SoundRGB object             //

  SoundRGB (float SoundFreq); 
  
// Set methods 

// Set method that implements the virtual method of the mother class //
 
  void SetSoundFreq(float SoundFreq);
  
// Access methods

// The following methods return respectively the R,G,B,A components  //
// of the SoundRGB object.                                           //
// no arguments                                                      //

  unsigned char  GetR();
  unsigned char  GetG();
  unsigned char  GetB();
  unsigned char  GetA();
  
// Get method that assigns the RGBA components of the SoundRGB       //
// object to the array of char passed as argument                    //
// argument:                                                         //
// RGBA: the array that has to be filled with the RGBA components    //

  void  GetRGBA(unsigned char *RGBA);
     

protected: 

// Protected methods

// This method assigns the RGBA components to a given array of char  //
// given a wavelength value. 
  void ColorWLtoRGBA(float WL, unsigned char RGBA[]);

// Protected members

  unsigned char  m_RGBA[4];

};

#endif

//Attenzione: le componenti rgb dei colori sono numeri interi a 8 bit



